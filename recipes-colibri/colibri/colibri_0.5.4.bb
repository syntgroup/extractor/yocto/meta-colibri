SUMMARY = "colibri UI application"
AUTHOR = "Timur Mayzenberg <t.mayzenberg@lambda-it.ru>"
HOMEPAGE = "https://gitlab.com/syntgroup/extractor/extractor-panel"
DESCRIPTION = "this recipe builds a Qt application for Colibri Extractor device"
LICENSE = "CLOSED"

DEPENDS += "qtbase qtserialport qttranslations qttools qtsvg qttools-native"
PV = "0.5.11+gitr${SRCPV}"
SRCREV = "16ed2cb351970abbba10d6b3249147821be99b46"
SRC_URI = "git://gitlab.com/syntgroup/extractor/extractor-panel.git;name=colibri;protocol=https;branch=666-test-build-yocto"

S = "${WORKDIR}/git"

inherit cmake_qt5
EXTRA_OECMAKE:append = "-DFETCHCONTENT_FULLY_DISCONNECTED=OFF -DBUILD_EMBEDDED=ON -DINSTALL_YOCTO=ON -DBUILD_SHARED_LIBS:BOOL=OFF"
do_configure[network] = "1"

FILES_${PN} += "/opt/syntol/bin/extractor-touchpanel"
FILES_${PN} += "/opt/syntol/bin/power-controls"
FILES_${PN} += "/opt/syntol/lib/*"