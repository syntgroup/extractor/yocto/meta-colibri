 
SUMMARY = "a console image"
LICENSE = "CLOSED"

IMAGE_FEATURES += "package-management"
IMAGE_LINGUAS = "en-us ru-ru"

# include recipes-core/images/rpi-basic-image.bb

CORE_OS = " \
  openssh openssh-keygen openssh-sftp-server \
  term-prompt \
  tzdata \
"

KERNEL_EXTRA = "\
  kernel-modules \
"

EXTRA_TOOLS = " \
  bzip2 \
  coreutils \
  curl \
  dosfstools \
  fbset \
  findutils \
  grep \
  i2c-tools \
  ifupdown \
  iptables \
  less \
  lsof \
  ntp ntp-tickadj \
  parted \
  unzip \
  wget \
  zip \
"

RPI_STUFF = " \
  userland \
"

SECURITY = " \
    checksec \
    firewall \
    nikto \
    nmap \
    python3-scapy \
    wireguard-tools \
"

SYSTEMD_STUFF = " \
    systemd-analyze \
    systemd-bash-completion \
"

WIFI = " \
    crda \
    iw \
    linux-firmware-rpidistro-bcm43430 \
    linux-firmware-rpidistro-bcm43455 \
    wpa-supplicant \
"

IMAGE_INSTALL += " \
  ${CORE_OS} \
  ${EXTRA_TOOLS} \
  ${RPI_STUFF} \
  ${SECURITY} \
  ${SYSTEMD_STUFF} \
  ${WIFI} \
  colibri \
"

set_local_timezone() {
  ln -sf /usr/share/zoneinfo/Europe/Moscow ${IMAGE_ROOTFS}/etc/localtime
  echo 'Europe/Moscow' > ${IMAGE_ROOTFS}/etc/timezone
}

create_opt_dir() {
  mkdir -p ${IMAGE_ROOTFS}/opt/syntol
}

ROOTFS_POSTPROCESS_COMMAND += " \
  set_local_timezone ; \
  create_opt_dir ; \
"

export IMAGE_BASENAME = "syntol-image"
